/**
 * MessageController
 *
 * @description :: Server-side logic for managing messages
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
 
var Slackbot = require('slackbot')

module.exports = {
  
  speak: function(req, res){
    
    var msg = req.query.message || "";
        
    res.render('speak', {message: msg}, function(err, html){
      res.send(html);
    });
    
  },
  
  post: function(req, res){
    
    var response = "Something went wrong";
    
    if(req.body.words && req.body.channel){
      
      var slackbot = new Slackbot('bunchball', 'FEaVnEClohCuLG8VKm1f8UUi'),
          channel = "#" + req.body.channel;
      
      slackbot.send(channel, req.body.words, function(err, slackSays, body){
        
        if(err){
          console.log(err);
          response = "Error " + err;
        }
        
        if(body === "ok"){
          response = "Message sent successfully";
        }
        
        res.redirect('/speak?message=' + response);
        
      });
      
    } else {
      
      var response = "We're missing something";
      
      res.redirect('/speak?message=' + response);
      
    }
    

  }
	
};

